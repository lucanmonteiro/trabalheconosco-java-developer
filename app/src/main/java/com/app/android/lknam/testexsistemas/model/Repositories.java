package com.app.android.lknam.testexsistemas.model;

/**
 * Created by Lucan Monteiro
 */

public class Repositories {

    private String name;
    private String language;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}