package com.app.android.lknam.testexsistemas.service;

import com.app.android.lknam.testexsistemas.model.Repositories;
import com.app.android.lknam.testexsistemas.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Lucan Monteiro
 */

public interface GitService {

    @GET("/users/{username}")
    Call<User> getUser(@Path("username") String username);

    @GET("/users/{username}/repos")
    Call<List<Repositories>> getRepos(@Path("username") String username);
}