package com.app.android.lknam.testexsistemas.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.android.lknam.testexsistemas.R;
import com.app.android.lknam.testexsistemas.adapter.AdapterRep;
import com.app.android.lknam.testexsistemas.model.Repositories;
import com.app.android.lknam.testexsistemas.service.GitService;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Lucan Monteiro
 */

@SuppressWarnings({"NullableProblems", "ConstantConditions"})
public class ResultActivity extends AppCompatActivity {

    private CircleImageView circleImageView;
    private TextView txUser;
    private RecyclerView recyclerView;
    private Retrofit retrofit;
    private Bundle b;
    private AdapterRep adapterRep;
    private List<Repositories> repositoriesList;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        initComp();

        if (b != null) {
            txUser.setText(b.getString("username"));
            Picasso.get().load(b.getString("photo")).into(circleImageView);

            GitService gitService = retrofit.create(GitService.class);
            Call<List<Repositories>> listRep = gitService.getRepos(b.getString("username"));

            listRep.enqueue(new Callback<List<Repositories>>() {
                @Override
                public void onResponse(Call<List<Repositories>> call, Response<List<Repositories>> response) {
                    progressBar.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);

                    if (response.isSuccessful()) {
                        repositoriesList = response.body();
                        for (int i = 0; i < repositoriesList.size(); i++) {
                            recyclerView.setHasFixedSize(true);
                            adapterRep = new AdapterRep(repositoriesList);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL);
                            dividerItemDecoration.setDrawable(getApplicationContext().getResources().getDrawable(R.drawable.divisor));
                            recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
                            recyclerView.setAdapter(adapterRep);
                        }

                    }
                }

                @Override
                public void onFailure(Call<List<Repositories>> call, Throwable t) {
                }
            });
        }
    }

    private void initComp() {
        Toolbar toolbar = findViewById(R.id.toolbarResult);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);

        circleImageView = findViewById(R.id.profile_image);
        txUser = findViewById(R.id.txUsername);
        recyclerView = findViewById(R.id.rv);
        progressBar = findViewById(R.id.progressBarResult);


        retrofit = new Retrofit.Builder().baseUrl("https://api.github.com").addConverterFactory(GsonConverterFactory.create()).build();
        b = getIntent().getExtras();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}