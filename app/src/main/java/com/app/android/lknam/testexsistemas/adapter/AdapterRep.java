package com.app.android.lknam.testexsistemas.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.android.lknam.testexsistemas.R;
import com.app.android.lknam.testexsistemas.model.Repositories;

import java.util.List;

/**
 * Created by Lucan Monteiro
 */

public class AdapterRep extends RecyclerView.Adapter<AdapterRep.MyViewHolder> {

    private List<Repositories> repositoriesList;

    public AdapterRep(List<Repositories> repositoriesList) {
        this.repositoriesList = repositoriesList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_rep, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Repositories repositories = repositoriesList.get(position);
        holder.name.setText(repositories.getName());
        holder.language.setText(repositories.getLanguage());
    }

    @Override
    public int getItemCount() {
        return repositoriesList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView name, language;

        private MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.txName);
            language = itemView.findViewById(R.id.txLang);
        }
    }
}