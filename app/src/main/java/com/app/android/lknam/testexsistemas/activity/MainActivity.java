package com.app.android.lknam.testexsistemas.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.app.android.lknam.testexsistemas.R;
import com.app.android.lknam.testexsistemas.model.User;
import com.app.android.lknam.testexsistemas.service.GitService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Lucan Monteiro
 */

@SuppressWarnings({"ConstantConditions", "NullableProblems"})
public class MainActivity extends AppCompatActivity {

    private Dialog alertDialog;
    private ConstraintLayout constraintLayout;
    private EditText editText;
    private Button button;
    private Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComp();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!editText.getText().toString().trim().isEmpty()) {
                    alertDialog.show();
                    searchUser(editText.getText().toString());
                } else editText.setError("Required field");
            }
        });
    }

    private void searchUser(String username) {
        GitService gitService = retrofit.create(GitService.class);
        Call<User> user = gitService.getUser(username);

        user.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                alertDialog.dismiss();

                if (response.isSuccessful()) {
                    User u = response.body();
                    Intent i = new Intent(MainActivity.this, ResultActivity.class);
                    i.putExtra("username", u.getLogin());
                    i.putExtra("photo", u.getAvatar_url());
                    startActivity(i);

                } else {
                    if (response.message().equals("Not Found")) showSnackBar("Usuário não encontrado.\nPor favor digite outro nome de usuário");
                    else
                        showSnackBar("Ops! Aconteceu um erro.\nVerifique sua conexão com a internet e tente novamente");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                alertDialog.dismiss();
                showSnackBar("Ops! Aconteceu um erro.\nVerifique sua conexão com a internet e tente novamente");
            }
        });
    }

    private void initComp() {
        constraintLayout = findViewById(R.id.clMain);
        editText = findViewById(R.id.editText);
        button = findViewById(R.id.button);

        alertDialog = new Dialog(this);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setCancelable(false);
        alertDialog.setContentView(R.layout.dialog_loading);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        retrofit = new Retrofit.Builder().baseUrl("https://api.github.com")
                .addConverterFactory(GsonConverterFactory.create()).build();
    }

    private void showSnackBar(String msg) {
        Snackbar.make(constraintLayout, msg, Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        }).show();
    }
}